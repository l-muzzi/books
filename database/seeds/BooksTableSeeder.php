<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Book::truncate();
        App\User::truncate();

//        return;

        $users = factory(App\User::class, 2)->create();

        foreach($users as $user) {
            $this->_createBooksForUser($user->id, 200);
        }

        $this->_createBooksForUser(null, 100);
    }

    private function _createBooksForUser($user_id, $k_quantity)
    {
        $books = app('db')->table('books');
        $time = date("Y-m-d H:i:s");

        for ($i = 0; $i < $k_quantity; $i++) {
            $list = [];
            for ($j = 0; $j <= 1000; $j++) {
                $isbn = rand(1000000, 9999999);
                $list[] = [
                    'name' => 'Book ' . $isbn,
                    'isbn' => $isbn,
                    'user_id' => $user_id,
                    'created_at' => $time,
                    'updated_at' => $time,
                ];
            }
            $books->insert($list);
        }
    }
}
