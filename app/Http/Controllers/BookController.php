<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use App\Book;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use App\Transformers\BookTransformer;
use App\Rules\Uppercase;
use Validator;

class BookController extends Controller
{
    private $fractal;
    private $bookTransformer;

    function __construct(Manager $fractal, BookTransformer $bookTransformer) {
        $this->fractal = $fractal;
        $this->bookTransformer = $bookTransformer;
    }

    public function index(Request $request) {
        $booksPaginator = Book::paginate(20);

        $resource = new Collection($booksPaginator->items(), $this->bookTransformer);
        $resource->setPaginator(new IlluminatePaginatorAdapter($booksPaginator));

//        $this->fractal->parseIncludes('user');

        return $this->fractal->createData($resource)->toArray();
    }

    public function show(Book $book)
    {
//        var_dump($book);exit;
        $resource = new Collection([$book], $this->bookTransformer);

        $this->fractal->parseIncludes('user');

        return $this->fractal->createData($resource)->toArray();
    }

    private function _validateInput($request) {
        $validator = Validator::make($request->all(), [
                'name' => ['required','min:3','max:100', new Uppercase],
                'isbn' => 'required|numeric',
            ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        }

        return true;
    }

    public function store(Request $request)
    {
        $result = $this->_validateInput($request);
        if($result !== true) {
            return $result;
        }

        $book = Book::create($request->all());

        return response()->json($book, 201);
    }

    public function update(Request $request, Book $book)
    {
        $result = $this->_validateInput($request);
        if($result !== true) {
            return $result;
        }

        $book->update($request->all());

        return response()->json($book, 200);
    }

    public function delete(Book $book)
    {
        $book->delete();

        return response()->json(null, 204);
    }
}