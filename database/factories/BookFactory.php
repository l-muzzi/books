<?php

use Faker\Generator as Faker;

$factory->define(App\Book::class, function (Faker $faker) {
    $isbn = rand(1000000, 9999999);
    return [
        'name' => 'Book '.$isbn,
        'isbn' => $isbn,
        'user_id' => 1
    ];
});
