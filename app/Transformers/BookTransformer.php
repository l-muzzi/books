<?php

namespace App\Transformers;

use App;
use App\Book;
use League\Fractal\TransformerAbstract;

class BookTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'user'
    ];

    public function transform(Book $book)
    {
        return [
            'id' => $book->id,
            'name' => $book->name,
            'isbn' => $book->isbn,
            'created_at' => $book->created_at?$book->created_at->toDateTimeString():null,
            'updated_at' => $book->updated_at?$book->updated_at->toDateTimeString():null,
            'deleted_at' => $book->deleted_at?$book->deleted_at->toDateTimeString():null,
            'user_id' => $book->user_id,
        ];
    }

    public function includeUser(Book $book)
    {
        return $this->item($book->user, App::make(UserTransformer::class));
    }
}