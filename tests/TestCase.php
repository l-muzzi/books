<?php

namespace Tests;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\Artisan;
use App\Book;
use App\User;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();
        Artisan::call('db:seed');
    }
    
    public function testsBooksAreCreatedCorrectly()
    {
        $user = factory(User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $payload = [
            'name' => 'LOREM',
            'isbn' => 1234,
        ];

        $this->json('POST', '/api/books', $payload, $headers)
            ->assertStatus(201)
            ->assertJson(['id' => 1, 'name' => 'LOREM', 'isbn' => 1234]);
    }

    public function testsBooksAreUpdatedCorrectly()
    {
        $user = factory(User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $book = factory(Book::class)->create([
            'name' => 'First Book',
            'isbn' => 5555,
        ]);

        $payload = [
            'name' => 'LOREM',
            'isbn' => 1234,
        ];

        $response = $this->json('PUT', '/api/books/' . $book->id, $payload, $headers)
            ->assertStatus(200)
            ->assertJson([ 
                'id' => 1, 
                'name' => 'LOREM',
                'isbn' => 1234,
            ]);
    }

    public function testsArtilcesAreDeletedCorrectly()
    {
        $user = factory(User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $book = factory(Book::class)->create([
            'name' => 'First Book',
            'isbn' => 5555,
        ]);

        $this->json('DELETE', '/api/books/' . $book->id, [], $headers)
            ->assertStatus(204);
    }
}
